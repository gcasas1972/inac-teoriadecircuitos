// question: 0  name: Switch category to $module$/top/magnetismo
$CATEGORY: $module$/top/magnetismo


// question: 51350  name: magnetismo
::magnetismo::[html]<p>que es el magnetismo</p>{
	~<p>Es una fuerza de atracción entre cargas&nbsp;</p>
	~<p>Es una fuerza que depende de la atracción de la gravedad</p>
	=<p>Se genera por la orientación de los electrones en los átomos&nbsp;</p>
	~<p>Todas las respuestas con afirmaciones son correctas</p>
	~<p>ninguna de las respuestas con afirmaciones es correcta</p>
}


// question: 51351  name: magnetismo
::magnetismo::[html]<p>que es el magnetismo</p>{
	~<p>Es una fuerza de atracción entre cargas&nbsp;</p>
	~<p>Es una fuerza que depende de la atracción de la gravedad</p>
	~<p>Se genera por la la cercanía de un material ferromagnético</p>
	~<p>Todas las respuestas con afirmaciones son correctas</p>
	=<p>ninguna de las respuestas con afirmaciones es correcta</p>
}


// question: 51352  name: magnetismo relaciones
::magnetismo relaciones::[html]<p>respecto de la relación de un imán con otros materiales se puede decir que&nbsp; es&nbsp;</p>{
	~<p>Ferromagnéticos\: Son materiales que se atraen fuertemente hacia los imanes.<br></p>
	~<p>Paramagnéticos\: Son materiales que también se atraen hacia los imanes, pero su atracción es mucho más débil que la de los materiales ferromagnéticos<br></p>
	~<p>Diamagnéticos\: Son materiales que se repelen de los imanes.<br></p>
	=<p>Todas las respuestas con afirmaciones son correctas</p>
	~<p>Ninguna de respuestas con afirmaciones es correcta</p>
}


// question: 51353  name: magnetismo relaciones
::magnetismo relaciones::[html]<p>respecto de la relación de un imán con otros materiales se puede decir que&nbsp; es&nbsp;</p>{
	=<p>Ferromagnéticos\: Son materiales que se atraen fuertemente hacia los imanes.<br></p>
	~<p>Paramagnéticos\:&nbsp;Son materiales que se repelen de los imanes.<br></p>
	~<p>Diamagnéticos\:&nbsp;Son materiales que también se atraen hacia los imanes, pero su atracción es mucho más débil que la de los materiales ferromagnéticos<br></p>
	~<p>Todas las respuestas con afirmaciones son correctas</p>
	~<p>Ninguna de respuestas  con afirmaciones es correcta</p>
}


// question: 51354  name: magnetismo relaciones
::magnetismo relaciones::[html]<p>respecto de la relación de un imán con otros materiales se puede decir que&nbsp; es&nbsp;</p>{
	~<p>Ferromagnéticos\:&nbsp;Son materiales que se repelen de los imanes.<br></p>
	=<p>Paramagnéticos\: Son materiales que también se atraen hacia los imanes, pero su atracción es mucho más débil que la de los materiales ferromagnéticos<br></p>
	~<p>Diamagnéticos\: Son materiales que se atraen fuertemente hacia los imanes.<br></p>
	~<p>Todas las respuestas con afirmaciones son correctas</p>
	~<p>Ninguna de respuestas con afirmaciones es correcta</p>
}


// question: 51355  name: magnetismo relaciones
::magnetismo relaciones::[html]<p>respecto de la relación de un imán con otros materiales se puede decir que&nbsp; es&nbsp;</p>{
	~<p>Ferromagnéticos\:&nbsp;Son materiales que también se atraen hacia los imanes, pero su atracción es mucho más débil que la de los materiales ferromagnéticos<br></p>
	~<p>Paramagnéticos\:&nbsp;Son materiales que se atraen fuertemente hacia los imanes.&nbsp;&nbsp;<br></p>
	=<p>Diamagnéticos\: Son materiales que se repelen de los imanes.<br></p>
	~<p>Todas las respuestas con afirmaciones son correctas</p>
	~<p>Ninguna de respuestas  con afirmaciones es correcta</p>
}


// question: 51356  name: magnetismo relaciones
::magnetismo relaciones::[html]<p>respecto de la relación de un imán con otros materiales se puede decir que&nbsp; es&nbsp;</p>{
	~<p>Ferromagnéticos\:&nbsp;Son materiales que se repelen de los imanes.<br></p>
	~<p>Paramagnéticos\:&nbsp; Son materiales que se atraen fuertemente hacia los imanes.&nbsp;<br></p>
	~<p>Diamagnéticos\:&nbsp; Son materiales que también se atraen hacia los imanes, pero su atracción es mucho más débil que la de los materiales ferromagnéticos.&nbsp;<br></p>
	~<p>Todas las respuestas con afirmaciones son correctas</p>
	=<p>Ninguna de respuestas  con afirmaciones es correcta</p>
}


// question: 51357  name: mono polo magnético
::mono polo magnético::[html]<p>definir cual de las siguientes opciones es correcta</p>{
	~<p>un imán esta formado por dos polos uno norte y uno sur</p>
	~<p>si rompo un imán al medio obtengo dos imanes</p>
	~<p>En el exterior del imán se extienden líneas de fuerza que van desde el norte hasta el sur.</p>
	=<p>Todas las respuestas con afirmaciones son correctas<br></p>
	~<p>Ninguna de respuestas con afirmaciones es correcta<br></p>
}


// question: 51358  name: mono polo magnético 2
::mono polo magnético 2::[html]<p>definir cual de las siguientes opciones es correcta</p>{
	=<p>un imán esta formado por dos polos uno norte y uno sur</p>
	~<p>si rompo un imán al medio obtengo un polo norte y un polo sur&nbsp;</p>
	~<p>En el exterior del imán no hay líneas de fuerza, solo en el interior</p>
	~<p>Todas las respuestas con afirmaciones son correctas<br></p>
	~<p>Ninguna de respuestas con afirmaciones es correcta<br></p>
}


// question: 51359  name: mono polo magnético 3
::mono polo magnético 3::[html]<p>definir cual de las siguientes opciones es correcta</p>{
	~<p>un imán esta formado por un solo polo o el norte o el sur</p>
	=<p>si rompo un imán al medio obtengo dos imanes</p>
	~<p>En el exterior del imán no hay líneas de fuerza&nbsp;</p>
	~<p>Todas las respuestas con afirmaciones son correctas<br></p>
	~<p>Ninguna de respuestas con afirmaciones es correcta<br></p>
}


// question: 51360  name: mono polo magnético 4
::mono polo magnético 4::[html]<p>definir cual de las siguientes opciones es correcta</p>{
	~<p>un imán esta formado por un solo polo el norte o el sur</p>
	~<p>si rompo un imán al medio obtengo un polo norte y un polo sur</p>
	=<p>En el exterior del imán se extienden líneas de fuerza que van desde el norte hasta el sur.</p>
	~<p>Todas las respuestas con afirmaciones son correctas<br></p>
	~<p>Ninguna de respuestas con afirmaciones es correcta<br></p>
}


// question: 51361  name: mono polo magnético 5
::mono polo magnético 5::[html]<p>definir cual de las siguientes opciones es correcta</p>{
	~<p>un imán esta formado por un solo polo el norte o el sur</p>
	~<p>si rompo un imán al medio obtengo un polo norte y un polo sur</p>
	~<p>En el exterior del imán no se extienden líneas de fuerza.</p>
	~<p>Todas las respuestas con afirmaciones son correctas<br></p>
	=<p>Ninguna de respuestas con afirmaciones es correcta<br></p>
}


// question: 0  name: Switch category to $module$/top/atomo
$CATEGORY: $module$/top/atomo


// question: 51213  name: que es
::que es::[html]que es un àtomo{
	~<p>Es una estructura de la materia</p>
	=<p>Es la unidad básica de la materia</p>
	~<p>un átomo es una unión química de varios compuestos</p>
	~<p>Una átomo es una aleación de metales</p>
	~<p>Ninguna de las afirmaciones es correcta</p>
}


// question: 51214  name: que es
::que es::[html]que es un àtomo{
	~<p>Es una estructura de la materia</p>
	~<p>Es la unión entre un metal y el oxìgeno</p>
	~<p>un átomo es una unión química de varios compuestos</p>
	~<p>Una átomo es una aleación de metales</p>
	=<p>Ninguna de las afirmaciones es correcta</p>
}


// question: 51215  name: que es
::que es::[html]<p>cual de todos los modelos expuestos, fue el primer modelo atómico</p>{
	=<p>modelo de Dalton</p>
	~<p>modelo de Thomson<br></p>
	~<p>modelo de Rutherford<br></p>
	~<p>modelo de Bohr<br></p>
	~<p>modelo de atómico cuántico<br></p>
}


// question: 0  name: Switch category to $module$/top/Ley de Faraday
$CATEGORY: $module$/top/Ley de Faraday


// question: 51362  name: ley de faraday
::ley de faraday::[html]<p>cual de las siguientes afirmaciones es correcta con relación a la ley de Faraday</p>{
	~<p>la fuerza electro motriz inducida depende de la variación del flujo magnético y la variación del tiempo.</p>
	~la cantidad de espiras en la bobina utilizada tiene un influencia directa
	~<p>El sentido de la corriente inducida, se puede determinar por la regla de la mano derecha</p>
	=<p>Todas las respuestas con afirmaciones son correctas<br></p>
	~<p>Ninguna de respuestas con afirmaciones es correcta<br></p>
}


// question: 51363  name: ley de faraday 2
::ley de faraday 2::[html]<p>cual de las siguientes afirmaciones es correcta con relación a la ley de Faraday</p>{
	=<p>la fuerza electro motriz inducida depende de la variación del flujo magnético y la variación del tiempo.</p>
	~la cantidad de espiras en la bobina utilizada no tiene un influencia directa
	~<p>El sentido de la corriente inducida, se puede determinar por la regla de la mano izquierda</p>
	~<p>Todas las respuestas con afirmaciones son correctas<br></p>
	~<p>Ninguna de respuestas con afirmaciones es correcta<br></p>
}


// question: 51364  name: ley de faraday 3
::ley de faraday 3::[html]<p>cual de las siguientes afirmaciones es correcta con relación a la ley de Faraday</p>{
	~<p>la fuerza electro motriz inducida depende de lo grande que sea el campo magnético</p>
	=la cantidad de espiras en la bobina utilizada&nbsp; tiene un influencia directa
	~<p>El sentido de la corriente inducida, se puede determinar por la regla de la mano izquierda</p>
	~<p>Todas las respuestas con afirmaciones son correctas<br></p>
	~<p>Ninguna de respuestas con afirmaciones es correcta<br></p>
}


// question: 51365  name: ley de faraday 4
::ley de faraday 4::[html]<p>cual de las siguientes afirmaciones es correcta con relación a la ley de Faraday</p>{
	~<p>la fuerza electro motriz inducida depende de lo grande que sea el campo magnético</p>
	~la cantidad de espiras en la bobina utilizada no tiene un influencia directa
	=<p>El sentido de la corriente inducida, se puede determinar por la regla de la mano derecha</p>
	~<p>Todas las respuestas con afirmaciones son correctas<br></p>
	~<p>Ninguna de respuestas con afirmaciones es correcta<br></p>
}


// question: 51366  name: ley de faraday 5
::ley de faraday 5::[html]<p>cual de las siguientes afirmaciones es correcta con relación a la ley de Faraday</p>{
	~<p>la fuerza electro motriz inducida depende de lo grande que sea el campo magnético</p>
	~la cantidad de espiras en la bobina utilizada no tiene un influencia directa
	~<p>El sentido de la corriente inducida, se puede determinar por la regla de la mano izquierda</p>
	~<p>Todas las respuestas con afirmaciones son correctas<br></p>
	=<p>Ninguna de respuestas con afirmaciones es correcta<br></p>
}


